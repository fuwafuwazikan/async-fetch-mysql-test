import Express from 'express';
import UsesController from '../controllers/api';

let router = Express.Router(),
  ctrler = new UsesController();

router.get('/', ctrler.index);

export default router;
