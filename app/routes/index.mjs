import Express from 'express';
import APIRouter from './api';
import UsesController from '../controllers/index';

let router = Express.Router(),
  ctrler = new UsesController();

router.use('/api', APIRouter);
router.get('/', ctrler.index);

export default router;
