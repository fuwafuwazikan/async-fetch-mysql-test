import BaseController from '../../base/controller';
export default class IndexController extends BaseController {
  index(req, res, next) {
    res.render('index', { title: 'Express' });
  }

  constructor() {
    super();
  }
}